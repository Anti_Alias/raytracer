package tracer
import Precision._
import MathUtil._
import scala.annotation.{tailrec}
import scala.collection.mutable.{ArrayBuffer}


/**
* Represents a mutable object that houses 3D objects in the
* ray-tracer.
*/
class Scene
{
	// ------------- FIELDS --------------
	var things: Set[Thing] = Set.empty
	var lights: Set[Light] = Set.empty
	val camera: Camera = Camera()
	var bgColor = Color.Black
	var ambientColor:Color = Color.Black
	var canSeeLights:Boolean = false


	/**
	* Ray-traces the scene, and draws it onto a Texture.
	* @param tex Texture to draw on.
	*/
	def trace(tex: Texture)
	{
		// Sets up variables related to viewport
		val nearPlane: Plane = camera.nearPlane						// Calculates the near plane of the Camera.
		val camOrigin: Vec = camera.pos								// Origin of the Camera
		val widthRatio: Decimal = camera.width / tex.width			// Ratio for pixels.
		val heightRatio: Decimal = camera.height / tex.height		// Ratio for pixels.
		val halfX: Vec = nearPlane.dirU.toUnit * widthRatio / 2
		val halfY: Vec = nearPlane.dirV.toUnit * heightRatio / 2
		val halfPixel: Vec = halfX + halfY
		
		// For every pixel in Texture...
		for(y <- (0 until tex.height).par)
		{
			for(x <- 0 until tex.width)
			{
				// Produces ray originating from the position on the near plane outwards.
				val pixelPos: Vec = nearPlane
					.interp(x/tex.width.toDouble, y/tex.height.toDouble)
					.+(halfPixel)
				val dir:Vec = pixelPos-camOrigin
				val ray = Ray(pixelPos, dir)

				var spec:Color = Color.Black
				if(canSeeLights)
				{
					for(light <- lights)
					{
						val pixelToLight:Vec = light.pos - pixelPos
						val dirScaled:Vec = dir.toUnit * pixelToLight.mag
						if(!intersects(Ray(pixelPos, dirScaled)))
						{
							val lightCos:Decimal = dir.toUnit dot pixelToLight.toUnit
							val bright:Decimal = pow(lightCos, 2048) * light.intensity * 1
							spec += light.col * bright
						}
					}
				}

				// Assigns the final color
				tex(x, y) = computeColor(ray, 3) + spec
			}
		}
	}


	/**
	* Computes the color that should be yielded
	* by a ray .
	*/
	def computeColor(ray: Ray, bounce:Int): Color =
	{
		// Gets all intersection with that ray in the scene sorted from closest to farthest.
		val inter: Intersection = closestIntersection(ray)

		// Returns either background color, or color of intersection
		if(inter != null)
		{
			// Sets up diffuse and specular variables
			var diffuse:Color = Color.Black
			var specular:Color = Color.Black

			// Gets information for intersection and reflected ray
			val interPos:Vec = inter.pos
			val interDir:Vec = inter.ray.dir
			val norm: Vec =
			{
				val temp:Vec = inter.norm
				val normPos: Vec = interPos + temp
				if((normPos-ray.origin).magSquared-1 <= (interPos-ray.origin).magSquared)
					temp
				else -temp
			}
			val reflectionDir:Vec = interDir - norm * (((interDir*2) dot norm)/norm.magSquared)
			
			// Calculates specular and shade values using given lights
			for(light:Light <- lights)
			{
				// Makes shade value 0 if itersects something on the way to the light
				val lightDir: Vec = light.pos - interPos
				val interToLight = Ray(interPos, lightDir)
				var bright:Decimal = 0
				var shade:Decimal = 0
				if(intersects(interToLight))
				{
					shade = 0
					bright = 0
				}

				// Otherwise, calculates shade for diffuse lighting.
				else
				{
					// Calculates shade
					val lightDirUnit: Vec = lightDir.toUnit
					shade = clamp(lightDirUnit dot norm) * light.intensity
			
					// Specular scalar
					val lightCos:Decimal = clamp(reflectionDir.toUnit dot lightDirUnit)
					bright = pow(lightCos, inter.exponent) * light.intensity * inter.specularReflect
				}

				diffuse += inter.sample * shade
				specular += (light.col * bright)
			}


			// Applies reflection to diffuse
			val diffuseWithReflection: Color =
				if(bounce != 0)
				{
					val bounceRay = Ray(interPos, reflectionDir)
					val reflectColor: Color = computeColor(bounceRay, bounce-1)
					Color.mix(diffuse, reflectColor, inter.reflect)
				}
				else diffuse

			// returns resulting color
			ambientColor + diffuseWithReflection + specular
		}
		else
		{
			bgColor
		}
	}


	/**
	* Determines if a ray
	*/
	def intersects(ray: Ray):Boolean = things.exists(_.intersects(ray))


	/**
	* Finds intersection with a Ray a every Thing in the Scene.
	*/
	def intersections(ray:Ray): Seq[Intersection] =
	{
		val buffer = new ArrayBuffer[Intersection]
		for(thing:Thing <- things)
		{
			val inter:Intersection = thing.intersect(ray)
			if(inter != null) buffer += inter
		}

		buffer.toSeq
	}


	def closestIntersection(ray:Ray): Intersection =
	{
		val all:Seq[Intersection] = intersections(ray)
		if(all.size == 0) null
		else
		{
			val closest:Intersection = all.reduce{(a, b) =>
				if(a.t < b.t) a else b
			}
			closest
		}
	}
}

object Scene
{
	def apply(): Scene = new Scene()
}
