package tracer
import Precision._
import Color._
import scala.util.{Random}
import java.io.{File}
import java.awt.{Image}
import java.awt.image.{BufferedImage}
import javax.imageio.{ImageIO}



/**
* Main class that fires the application up
*/
object Main extends App
{
	// Deletes all images
	val files = new File(".").listFiles
	for(file <- files) if(file.getPath.endsWith(".png")) file.delete

	// Makes Spheres
	val rand = new Random(123)

	// Makes origins and velocities for spheres
	val numSpheres = 21
	val origins = (for(i <- 0 until numSpheres) yield (Vec.random(rand)*20 - Vec(10, 10, 10))).toSeq
	val directions:Seq[Vec] = (for(i <- 0 until numSpheres) yield (Vec.random(rand) - Vec.One/2)*40).toSeq

	// Makes spheres
	val spheres:Seq[Thing] = origins.map{origin:Vec =>
		val rad: Decimal = rand.nextDouble*3 + 1
		val r: Decimal = rand.nextDouble
		val g: Decimal = rand.nextDouble
		val b: Decimal = rand.nextDouble
		Sphere(origin, rad, Color(r, g, b))
	}


	// Makes platform
	val things: Set[Thing] = Set[Thing](
		AABB(Vec(-80, -80, -80), Vec(160, 160, 160), Color.Red, Color.Blue, Color.Yellow, Color.Green, Color.Cyan, Color.White),
		AABB(Vec(-50, -80, -50), Vec(10, 90, 10), Color.Yellow),
		AABB(Vec(50, -80, -50), Vec(10, 90, 10), Color.White),
		AABB(Vec(50, -80, 50), Vec(10, 90, 10), Color.Purple),
		AABB(Vec(-50, -80, 50), Vec(10, 90, 10), Color.Pink)
	) ++ spheres.toSet

	// Makes Lights
	val numLights = 1
	val lights:Set[Light] = Set(
		Light(Vec(60, 50, 0), Color.White, 0.5),
		Light(Vec(-60, -50, 0), Color.White, 0.5)
	)

	// Makes a Scene ;)
	val scene = Scene()
	val cam: Camera = scene.camera
	cam.nearPlaneDistance = 10
	cam.width = 16
	cam.height = 9
	scene.things = things
	scene.lights = lights
	scene.bgColor = Color.Black
	scene.ambientColor = Color(0.1, 0.1, 0.1, 1)
	
	// Renders frames.
	val numFrames = 180
	val startTime:Long = System.currentTimeMillis
	var camDist = 60
	for(i <- 0 until numFrames)
	{
		// Positions and orients camera
		val angle: Decimal = math.Pi*2*(i/numFrames.toDouble)
		val x: Decimal = math.cos(angle) * camDist
		val y: Decimal = Math.sin(angle*2)*40
		val z: Decimal = math.sin(angle) * camDist
		cam.pos = Vec(x, y, z)
		cam.lookAt(Vec(0, 0, 0))

		// Moves spheres
		for((s, origin, dir) <- (spheres, origins, directions).zipped)
		{
			val t:Decimal = math.sin(angle)
			s.origin = origin + (dir*t)
		}

		// Makes image/texture.  They both share the same Int buffer.
		val resScale = args(0).toInt
		val bimg = new BufferedImage(16*resScale, 9*resScale, BufferedImage.TYPE_INT_ARGB)
		val tex = Texture(bimg)

		// Ray traces scene onto texture
		val traceTimeStart:Long = System.currentTimeMillis
		scene.trace(tex)
		val traceTimeElapsed:Long = System.currentTimeMillis - traceTimeStart
		val traceSeconds = traceTimeElapsed/1000.0
		
		// Prints out progress
		val currentFrame = i+1
		val percDone = math.round(currentFrame/numFrames.toDouble*100)
		println(s"Frames: ($currentFrame/$numFrames) $percDone% (Took $traceSeconds seconds to trace)")

		// Writes texture to file.
		val numId = padLeft(("" + i), "0", 5)
		new Thread(new Runnable()
		{
			override def run():Unit = ImageIO.write(bimg, "PNG", new File(s"frame$numId.png"))
		}).start()
	}
	val elapsedMillis:Long = System.currentTimeMillis - startTime
	val elapsedSeconds:Long = elapsedMillis / 1000
	println(s"Done in $elapsedSeconds seconds")

	// Compiles
	val animFile: File = new File("animation.mp4")
	if(animFile.exists) animFile.delete
	println("Compiling into animation.mp4...")
	Runtime.getRuntime.exec("ffmpeg -r 30 -f image2 -i frame%05d.png -vcodec libx264 -crf 2  -pix_fmt yuv420p animation.mp4")
	println("Done!")


	// ---------------------------- METHODS ------------------------------------
	def padLeft(str: String, char: String, len: Int):String = if(str.length >= len) str else
	{
		val extra = char + str
		padLeft(extra, char, len)
	}
}
