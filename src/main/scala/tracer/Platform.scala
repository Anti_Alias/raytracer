package tracer
import Precision._


/**
* Represents a Thing that is an Axis-Aligned-Bounding-Box
* @param origin Bottom-left-far corner of the AABB
* @param color Color of the Platform
*/
class Platform(origin: Vec, var color: Color) extends Thing(origin)
{
	var exponent:Decimal = 64
	var reflect:Decimal = 0.2

	// OVERRIDDEN METHODS
	override def intersect(ray: Ray):Intersection =
	{
		val t: Decimal = (origin.y - ray.origin.y)/ray.dir.y
		if(t > 0.0000001) new Intersection(ray, t, this)
		{
			override def norm = Vec(0, 1, 0)
			override def sample:Color = color
			override def exponent:Decimal = Platform.this.exponent
			override def reflect:Decimal = Platform.this.reflect
		}
		else null
	}

	override def intersects(ray: Ray):Boolean =
	{
		val t: Decimal = (origin.y - ray.origin.y)/ray.dir.y
		t > 0.0000001 && t < 1
	}
}


/**
* Companion object
*/
object Platform
{
	def apply(origin:Vec, color:Color):Platform = new Platform(origin, color)
}
