package tracer
import Precision._


/**
* Represents a plane in 3D space
*/
case class Plane(origin: Vec, dirU: Vec, dirV: Vec)
{
	/**
	* Interpolates along the Plane
	*/
	def interp(u: Decimal, v: Decimal): Vec = origin + dirU*u + dirV*v
	def center: Vec = interp(0.5, 0.5)
	def endPointU: Vec = origin + dirU
	def endPointV: Vec = origin + dirV
	def width: Decimal = dirU.mag
	def height: Decimal = dirV.mag
}
