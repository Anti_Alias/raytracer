package tracer
import Precision._
import MathUtil._


/**
* Represents a Color which uses an underlying Int.
* Compiles to an Int because it extends AnyVal.
*/
case class Color(underlying: Int) extends AnyVal
{
	def a: Int = (underlying >>> 24) & 0xFF
	def r: Int = (underlying >>> 16) & 0xFF
	def g: Int = (underlying >>> 8) & 0xFF
	def b: Int = underlying & 0xFF
	def rR:Decimal = r/255.0
	def gR:Decimal = g/255.0
	def bR:Decimal = b/255.0
	def aR:Decimal = a/255.0
	def +(c: Color) = Color(r+c.r, g+c.g, b+c.b, a+c.a)
	def -(c: Color) = Color(r-c.r, g-c.g, b-c.b, a-c.a)
	def *(c: Color) = Color(rR*c.rR, gR*c.gR, bR*c.bR, aR*c.aR)
	def *(s: Decimal) = Color(r*s/255, g*s/255, b*s/255, a*s/255)
	def /(c: Color) = Color(r/c.r, g/c.g, b/c.b, a/c.a)
	def /(s: Decimal):Color =
	{
		val inv: Decimal = 1/s
		this * s
	}

	/**
	* Opaque version of this color such that its alpha
	* value is 255.
	*/
	def opaque: Color = Color(r, g, b, 255)
	def mix(c: Color) = Color.mix(this, c, 0.5)
	override def toString: String = s"Color(r:$r, g:$g, b:$b a:$a)"
	def toHexString: String = Integer.toHexString(underlying)
}


/**
* Stores useful Color constants.
*/
object Color
{
	def apply(r: Decimal, g: Decimal, b: Decimal, a: Decimal):Color =
	{
		val rInt = (clamp(r) * 255).toInt
		val gInt = (clamp(g) * 255).toInt
		val bInt = (clamp(b) * 255).toInt
		val aInt = (clamp(a) * 255).toInt
		Color((aInt << 24) + (rInt << 16) + (gInt << 8) + bInt)
	}
	def apply(r: Int, g: Int, b: Int, a:Int):Color =
	{
		val rInt = clamp(r, 255).toInt
		val gInt = clamp(g, 255).toInt
		val bInt = clamp(b, 255).toInt
		val aInt = clamp(a, 255).toInt
		Color((aInt << 24) + (rInt << 16) + (gInt << 8) + bInt)
	}

	def apply(r:Int, g:Int, b:Int):Color = apply(r, g, b, 255)
	def apply(r:Decimal, g:Decimal, b:Decimal):Color = apply(r, g, b, 1)

	def mix(a: Color, b: Color, t: Decimal):Color = Color(
		(a.r + (b.r-a.r)*t).toInt,
		(a.g + (b.g-a.g)*t).toInt,
		(a.b + (b.b-a.b)*t).toInt,
		(a.a + (b.a-a.a)*t).toInt
	)
	val Red = Color(0xFFFF0000)
	val Green = Color(0xFF00FF00)
	val Blue = Color(0xFF0000FF)
	val Yellow = Color(0xFFFFFF00)
	val Magenta = Color(0xFFFF00FF)
	val Cyan = Color(0xFF00FFFF)
	val White = Color(0xFFFFFFFF)
	val Black = Color(0xFF000000)
	val Gray = Color(0.5, 0.5, 0.5)
	val Purple = Color(0x009B30FF)
	val Pink = Red mix White
}
