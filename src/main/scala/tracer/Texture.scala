package tracer
import java.awt.{Image, Graphics2D}
import java.awt.image.{BufferedImage, DataBufferInt}
import javax.imageio.{ImageIO}
import java.net.{URL}
import Precision._
import Texture._


/**
* Represents a Texture object.
* Can be used for sampling, or 
* file IO.
* @param Int buffer used to store pixels.
*/
class Texture(val buff: Array[Int], val width: Int, val height: Int)
{
	// Checks args
	require(width >= 0 && height >= 0 && width*height == buff.length, "Invalid texture size")
	var sampler: Sampler = NearestNeighbor
	
	// ------------------- METHODS --------------------------------
	def apply(x: Int, y: Int) = Color(sampler(x, y, width, height, buff))
	def apply(x: Decimal, y: Decimal): Color = apply(x.toInt * width, y.toInt * height)
	def apply(v: Vec): Color = apply(v.x, v.y)

	def update(x: Int, y: Int, col: Color):Unit = buff(y*width + x) = col.underlying
	def update(x: Decimal, y: Decimal, col: Color):Unit = update(x.toInt * width, y.toInt * height, col)
	def update(v: Vec, col: Color): Unit = update(v.x, v.y, col)
}

/**
* Helper class
*/
object Texture
{
	/**
	* Type of function that takes three arguments (x, y, width of buffer, height of buffer and buffer itself),
	* and returns a sample as an Int
	*/
	trait Sampler
	{
		def apply(x: Int, y: Int, width: Int, height: Int, buff: Array[Int]): Int
	}

	def apply(buff: Array[Int], width: Int, height: Int): Texture = new Texture(buff, width, height)
	def apply(img: BufferedImage): Texture =
	{
		val intBuffer: Array[Int] = img
			.getRaster
			.getDataBuffer
			.asInstanceOf[DataBufferInt]
			.getData
		new Texture(intBuffer, img.getWidth, img.getHeight)
	}
	def apply(url: URL): Texture =
	{
		val img: BufferedImage = ImageIO.read(url)
		val newImage = new BufferedImage(img.getWidth, img.getHeight, BufferedImage.TYPE_INT_ARGB)
		val g = newImage
			.getGraphics
			.asInstanceOf[Graphics2D]
		g.drawImage(img, null, 0, 0)
		apply(newImage)
	}

	/**
	* Default implementation of Sampler.
	*/
	val NearestNeighbor:Sampler = new Sampler()
	{
		override def apply(x: Int, y: Int, width: Int, height: Int, buff:Array[Int]):Int = buff(y*width + x)
	}
}
