package tracer
import scala.language.{implicitConversions}


/**
* Object that determines the decimal precision to use
*/
object Precision
{
	type Decimal = Double
	implicit def double2Float(d: Double): Float = d.toFloat
}
