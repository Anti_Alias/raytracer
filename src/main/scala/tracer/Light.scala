package tracer
import Precision._


/**
* Represents a light coming from a single source.
* @param pos Position of the light
* @param col Color of the light
* @param intensity How bright the light is.
*/
class Light(var pos:Vec, var col:Color, var intensity:Decimal=0.5)
object Light
{
	def apply(pos:Vec, col:Color, intensity:Decimal):Light = new Light(pos, col, intensity)
}
