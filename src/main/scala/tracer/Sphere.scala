package tracer
import Precision._
import MathUtil._



/**
* Implementation of Thing as a Sphere.
*/
class Sphere(org:Vec, var radius:Decimal, var color:Color) extends Thing(org)
{
	var maybeTex: Option[Texture] = None
	var exponent: Decimal = 256
	var reflect: Decimal = 0.1
	var specularReflect:Decimal = 3


	/**
	* Possible intersects this Sphere with a Ray
	*/
	override def intersect(r: Ray):Intersection =
	{
		// Unpacks/calculates variables
		val d: Vec = r.dir
		val o: Vec = origin
		val p: Vec = r.origin

		// Creates variables for quadratic formula
		val a: Decimal = d.magSquared
		val b: Decimal = 2 * (d dot (p-o))
		val c: Decimal = o.magSquared + p.magSquared - 2*(o dot p) - radius*radius
		val descr: Decimal = b*b-4*a*c

		// None if it does not intersect...
		if(descr < 0) null

		// Otherwise, calculate t, and return intersection point
		else
		{
			val part: Decimal = -math.sqrt(descr)
			val twoA: Decimal = 2*a
			val t1: Decimal = (-b-part) / twoA
			val t2: Decimal = (-b+part) / twoA
			val t = if(t1 < 0) t2 else if(t2 < 0) t1 else min(t1, t2)
			if(t > epsilon)
			{
				val interPos: Vec = p + d*t
				val interNorm: Vec = (interPos - origin).toUnit
				new Intersection(r, t, this)
				{
					override def pos: Vec = interPos
					override def norm: Vec = interNorm
					override def sample: Color =
					{
						if(maybeTex.isDefined) color
						else
						{
							color
						}
					}
					override def exponent:Decimal = Sphere.this.exponent
					override def reflect:Decimal = Sphere.this.reflect
					override def specularReflect:Decimal = Sphere.this.specularReflect
				}
			}
			else null
		}
	}


	override def intersects(r: Ray): Boolean =
	{
		// Unpacks/calculates variables
		val d: Vec = r.dir
		val o: Vec = origin
		val p: Vec = r.origin

		// Creates variables for quadratic formula
		val a: Decimal = d.magSquared
		val b: Decimal = 2 * (d dot (p-o))
		val c: Decimal = o.magSquared + p.magSquared - 2*(o dot p) - radius*radius
		val descr: Decimal = b*b-4*a*c

		// None if it does not intersect...
		if(descr < 0) false
		else
		{
			val part: Decimal = -math.sqrt(descr)
			val twoA: Decimal = 2*a
			val t1: Decimal = (-b-part) / twoA
			val t2: Decimal = (-b+part) / twoA
			val t = if(t1 < 0) t2 else if(t2 < 0) t1 else min(t1, t2)
			t > epsilon && t < 1
		}
	}

	override def toString: String = s"Sphere($origin, $radius, $color)"
}


/**
* Companion object
*/
object Sphere
{
	def apply(origin:Vec=Vec.Zero, radius:Decimal=10, color:Color=Color.Red):Sphere = new Sphere(origin, radius, color)
}
