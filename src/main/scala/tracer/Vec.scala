package tracer
import Precision._
import scala.util.{Random}


/**
* 3-Dimensional Vector
*/
case class Vec(x: Decimal, y: Decimal, z: Decimal)
{
	def +(v: Vec) = Vec(x+v.x, y+v.y, z+v.z)
	def -(v: Vec) = Vec(x-v.x, y-v.y, z-v.z)
	def *(v: Vec) = Vec(x*v.x, y*v.y, z*v.z)
	def /(v: Vec) = Vec(x/v.x, y/v.y, z/v.z)
	def *(s: Decimal) = Vec(x*s, y*s, z*s)
	def /(s: Decimal) =
	{
		val inv: Decimal = 1/s
		this * inv
	}
	def toUnit: Vec = this/mag
	def unary_- = Vec(-x, -y, -z)
	def *:(d: Decimal) = this*d
	def dot(v: Vec): Decimal = x*v.x + y*v.y + z*v.z
	def cross(v: Vec): Vec = Vec(y*v.z-z*v.y, -(x*v.z-z*v.x), x*v.y-y*v.x)
	def magSquared: Decimal = x*x + y*y + z*z
	def mag: Decimal = math.sqrt(magSquared)
}


/**
* Stores useful Vec constants
*/
object Vec
{
	val Left = Vec(-1, 0, 0)
	val Right = Vec(1, 0, 0)
	val Up = Vec(0, 1, 0)
	val Down = Vec(0, -1, 0)
	val Near = Vec(0, 0, 1)
	val Far = Vec(0, 0, -1)
	val Zero = Vec(0, 0, 0)
	val One = Vec(1, 1, 1)
	def random(r: Random):Vec = Vec(r.nextDouble, r.nextDouble, r.nextDouble)
}
