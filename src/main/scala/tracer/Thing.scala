package tracer
import Precision._
import MathUtil._


/**
* Represents a mutable 3D object in a Scene that is to be
* ray-traced.
*/
abstract class Thing(var origin: Vec = Vec.Zero)
{
	/**
	* Finds a possible intersection with the Ray provided.
	*/
	def intersect(r: Ray): Intersection

	/**
	* Method that verifies if a ray intersects this Thing
	*/
	def intersects(r: Ray): Boolean =
	{
		val result:Intersection = intersect(r)
		if(result == null) false
		else result.t >= epsilon && result.t<=1
	}
}
