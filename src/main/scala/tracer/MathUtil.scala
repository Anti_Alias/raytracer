package tracer
import Precision._


/**
* Object that contains many helpful math utility methods.
*/
object MathUtil
{
	def clamp(d: Decimal): Decimal = if(d < 0) 0 else if(d > 1) 1 else d
	def clamp(d: Decimal, max: Decimal): Decimal = if(d < 0) 0 else if(d > max) max else d
	def clamp(d: Decimal, min: Decimal, max: Decimal) = if(d < min) min else if(d > max) max else d

	def clamp(i: Int): Int = if(i < 0) 0 else if(i > 1) 1 else i
	def clamp(i: Int, max: Int):Int = if(i < 0) 0 else if(i > max) max else i
	def clamp(i: Int, min: Int, max: Int):Int = if(i < min) min else if(i > max) max else i

	def min(a:Decimal, b:Decimal):Decimal = if(a<b) a else b
	def max(a:Decimal, b:Decimal):Decimal = if(a>b) a else b


	def fastPow(a:Decimal, b:Int):Decimal =
	{
		var temp = a
		var i = 2
		while(i < b)
		{
			temp *= temp
			i *= 2
		}
		temp
	}


	/**
	* Power optimized for powers of two
	*/
	def pow(a:Decimal, b:Decimal):Decimal =
	{
		if(b % 1 == 0)
		{
			val intB:Int = b.toInt
			if(isPowerOfTwo(intB)) fastPow(a, intB)
			else math.pow(a, b)
		}
		else math.pow(a, b)
	}

	def isPowerOfTwo(i:Int):Boolean = i>0 && (i & (i-1)) == 0

	val epsilon:Decimal = 0.0000000001
}
