package tracer
import Precision._
import AABB._
import MathUtil.epsilon


/**
* Represents a Thing that is an Axis-Aligned-Bounding-Box
* @param origin Bottom-left-far corner of the AABB
* @param size Size of the AABB.
*/
class AABB(org: Vec, var size: Vec, var leftColor:Color, var rightColor:Color, var topColor:Color, var bottomColor:Color, var nearColor:Color, var farColor:Color) extends Thing(org)
{
	var exponent: Decimal = 1024
	var reflect: Decimal = 0.2
	var specularReflect:Decimal = 1.5

	// CORNER METHODS
	def blf: Vec = origin
	def brf: Vec = Vec(origin.x+size.x, origin.y, origin.z)
	def tlf: Vec = Vec(origin.x, origin.y+size.y, origin.z)
	def trf: Vec = Vec(origin.x+size.x, origin.y+size.y, origin.z)

	def bln: Vec = Vec(origin.x, origin.y, origin.z+size.z)
	def brn: Vec = Vec(origin.x+size.x, origin.y, origin.z+size.z)
	def tln: Vec = Vec(origin.x, origin.y+size.y, origin.z+size.z)
	def trn: Vec = origin + size

	def minX: Decimal = origin.x
	def maxX: Decimal = origin.x + size.x
	def minY: Decimal = origin.y
	def maxY: Decimal = origin.y + size.y
	def minZ: Decimal = origin.z
	def maxZ: Decimal = origin.z + size.z

	// OVERRIDDEN METHODS
	override def intersect(ray: Ray):Intersection =
	{
		val maybeTop:Intersection = intersectXZ(ray, origin.y+size.y, topColor)
		val maybeBottom:Intersection = intersectXZ(ray, origin.y, bottomColor)
		val maybeNear:Intersection = intersectXY(ray, origin.z+size.z, nearColor)
		val maybeFar:Intersection = intersectXY(ray, origin.z, farColor)
		val maybeRight:Intersection = intersectYZ(ray, origin.x+size.z, rightColor)
		val maybeLeft:Intersection = intersectYZ(ray, origin.x, leftColor)
		val minInter = min(maybeLeft, min(maybeRight, min(maybeFar, min(maybeNear, min(maybeBottom, maybeTop)))))
		minInter
	}

	def min(maybeA:Intersection, maybeB:Intersection):Intersection =
	{
		if(maybeA == null) maybeB
		else if(maybeB == null) maybeA
		else if(maybeA.t < maybeB.t) maybeA
		else maybeB
	}


	def intersectXZ(ray:Ray, y:Decimal, col:Color):Intersection =
	{
		val t: Decimal = (y - ray.origin.y)/ray.dir.y
		val point: Vec = ray.interp(t)

		if(t > epsilon && point.x>=minX && point.x<=maxX && point.z>=minZ && point.z<=maxZ)
			new AABBIntersection(ray, t, Vec.Up, col)
		else
			null
	}

	def intersectXY(ray:Ray, z:Decimal, col:Color):Intersection =
	{
		val t: Decimal = (z - ray.origin.z)/ray.dir.z
		val point: Vec = ray.interp(t)

		if(t > epsilon && point.x>=minX && point.x<=maxX && point.y>=minY && point.y<=maxY)
			new AABBIntersection(ray, t, Vec.Near, col)
		else
			null
	}

	def intersectYZ(ray:Ray, x:Decimal, col:Color):Intersection =
	{
		val t: Decimal = (x - ray.origin.x)/ray.dir.x
		val point: Vec = ray.interp(t)

		if(t > epsilon && point.y>=minY && point.y<=maxY && point.z>=minZ && point.z<=maxZ)
			new AABBIntersection(ray, t, Vec.Right, col)
		else
			null
	}


	class AABBIntersection(ray:Ray, t:Decimal, override val norm: Vec, override val sample:Color) extends Intersection(ray, t, AABB.this)
	{
		override def exponent:Decimal = AABB.this.exponent
		override def reflect:Decimal = AABB.this.reflect
		override def specularReflect:Decimal = AABB.this.specularReflect
	}
}


/**
* Companion object
*/
object AABB
{
	def apply(origin:Vec, size:Vec, color: Color):AABB = new AABB(origin, size, color, color, color, color, color, color)
	def apply(origin:Vec, size:Vec, leftColor:Color, rightColor:Color, topColor:Color, bottomColor:Color, nearColor:Color, farColor:Color):AABB =
		new AABB(origin, size, leftColor, rightColor, topColor, bottomColor, nearColor, farColor)
}
