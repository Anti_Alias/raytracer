package tracer
import Precision._


/**
* Represents the Camera in a Scene
* @param pos Position of the Camera
* @param dir Direction the Camera is facing
* @param up 'Up' vector that orients the Camera
* @param size Size of the camera's viewport
*/
class Camera(var pos: Vec, var dir: Vec, var width:Decimal, var height:Decimal, var upOrient: Vec)
{
	var nearPlaneDistance:Decimal = 10
	def forward: Vec = dir.toUnit
	def backward: Vec = -forward
	def left: Vec = (upOrient cross dir).toUnit
	def right: Vec = -left
	def up: Vec = (dir cross left).toUnit
	def down: Vec = -up

	

	/**
	* Calculates near plane of the Camera
	*/
	def nearPlane: Plane =
	{
		val center: Vec = (pos + dir)
		val posToCenter: Vec = center - pos
		val leftDir: Vec = (upOrient cross posToCenter).toUnit * width/2
		val upDir: Vec = (posToCenter cross leftDir).toUnit * height/2
		val tl: Vec = center + leftDir + upDir
		val u: Vec = -leftDir * 2
		val v: Vec = -upDir * 2
		Plane(tl, u, v)
	}


	/**
	* Has the Camera look at a position
	*/
	def lookAt(point: Vec):Unit =
	{
		dir = (point - pos).toUnit * nearPlaneDistance
	} 
}

object Camera
{
	def apply(pos:Vec = Vec(0, 0, 20), dir:Vec = Vec(0, 0, -10), width:Decimal=16, height:Decimal=9, upOrient:Vec=Vec.Up):Camera =
		new Camera(pos, dir, width, height, upOrient)
}
