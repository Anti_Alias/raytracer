package tracer
import Precision._


/**
* Represents information that was gathered when a ray intersects a Thing.
* @param ray Ray that was cast that hit some 'Thing'
* @param t T value that determines where on the ray the intersection occurred.
* @param thing Thing that was intersected with
*/
abstract class Intersection(val ray: Ray, val t: Decimal, val thing: Thing)
{
	/**
	* Position of the intersection.
	*/
	def pos: Vec = ray.interp(t)

	/**
	* Normal of the surface intersected.
	*/
	def norm: Vec

	/**
	* Sample color of the intersection.
	*/
	def sample: Color

	/**
	* Exponent used in specular lighting
	*/
	def exponent: Decimal
	
	/**
	* Reflectiveness of the surface.  Should
	* be between 0 and 1.  0 means no reflection,
	* while 1 means complete reflection.
	*/
	def reflect: Decimal


	/**
	* Reflectiveness of specular light.
	* Defaults to reflect.
	*/
	def specularReflect: Decimal = reflect
}
