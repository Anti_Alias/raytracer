package tracer
import Precision._

/**
* Represents a ray casted.
* @param origin Origin of the Ray
* @param dir Direction of the Ray
*/
case class Ray(origin: Vec, dir: Vec)
{
	/**
	* Interpolates between the origin of the ray,
	* and its direction.
	*/
	def interp(t: Decimal): Vec = origin + dir * t

	/**
	* origin + dir
	*/
	def endPoint: Vec = origin + dir

	/**
	* Imaginary length squared of the Ray.  Same as dir.magSquared
	*/
	def lenSquared: Decimal = dir.magSquared

	/**
	* Imaginary length of the Ray.  Same as dir.mag
	*/
	def len: Decimal = dir.mag
}
